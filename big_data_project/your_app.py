# streamlit_app.py

import streamlit as st

# Titre de l'application
st.title('Application Streamlit Simple')

# Bouton
if st.button('Cliquez ici'):
    st.write('Vous avez cliqué sur le bouton.')
