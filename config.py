

# config.py

# Configuration pour le client de l'API Dataiku
API_MODEL_ENDPOINT = "https://api-61123218-fad383b0-dku.eu-west-3.app.dataiku.io/"
API_MODEL_ID = "website_logs"

# URL pour obtenir des données en temps réel
URL_REAL_TIME_DATA = "https://api-56ce4d5f-779f448b-dku.eu-west-3.app.dataiku.io/public/api/v1/generate_rows/generate-rows/run"
