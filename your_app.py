import streamlit as st
from producer import get_real_time_data, push_data_kafka
from predict import predict_record
from consumer import poll_message
from time import sleep
import pandas as pd

# Titre de l'application
st.title('Application Streamlit Simple')

# Créer deux colonnes avec une largeur relative de 2:1
col1, col2 = st.columns([2, 1])

# Initialiser un dictionnaire pour compter les visites par emplacement
location_counts = {}

# Colonne de gauche : pour charger de nouvelles données
with col1:
    st.write("Charger de nouvelles données")
    
    # Sélectionneur de nombre avec une plage de 1 à 10
    number = st.number_input('Choisissez un nombre de données à charger', min_value=1, max_value=10, step=1, key='number_input')
    
    # Liste pour stocker les coordonnées de géolocalisation
    locations = []
    
    # Bouton de chargement
    if st.button('Charger les données', key='load_data'):
        # Appel de la fonction get_real_time_data() pour chaque nouvelle donnée
        for _ in range(number):
            new_data = get_real_time_data()
            
            # Extraire les coordonnées de géolocalisation
            geopoint = new_data.get("ip_adress_geopoint", "POINT(0 0)")
            # Format attendu "POINT(lon lat)"
            lon, lat = map(float, geopoint.strip("POINT()").split())
            
            # Ajouter les coordonnées à la liste
            locations.append({'lat': lat, 'lon': lon})
            
            # Compter les visites par emplacement
            location = new_data.get("location", "Inconnu")  # Utilisez une valeur par défaut si nécessaire
            if location in location_counts:
                location_counts[location] += 1
            else:
                location_counts[location] = 1
            
            # Afficher la nouvelle donnée
            st.write(new_data)
            sleep(1)  # Pause pour simuler le temps de chargement

            push_data_kafka(new_data)
            st.write("Données envoyées à Kafka")
        
        # Créer un DataFrame à partir de la liste des coordonnées et l'afficher sur une carte
        if locations:
            df_locations = pd.DataFrame(locations)
            st.map(df_locations, use_container_width=True)
            
        # Afficher le graphique à barres pour les visites par emplacement
        if location_counts:
            df_location_counts = pd.DataFrame(list(location_counts.items()), columns=["Location", "Count"])
            st.bar_chart(df_location_counts.set_index("Location"), use_container_width=True)


# Trait vertical de séparation
st.markdown("""<hr style="height:2px;border:none;color:#333;background-color:#333;" /> """, unsafe_allow_html=True)

# Colonne de droite : pour lancer le modèle
with col2:
    st.write("Lancer le modèle")
    
    # Bouton pour lancer le modèle
    if st.button('Lancer le modèle', key='model'):
        data_consume = poll_message()
        if data_consume == None:
            st.write("Génération de nouvelles données")
        else:
            st.write("Données chargées depuis Kafka")
            
            prediction = predict_record(data_consume)
            st.write("Prédiction du modèle : ", prediction["result"])

        
