FROM python:3.8-slim
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt


COPY . /app
WORKDIR /app
EXPOSE 8501
CMD ["streamlit", "run", "your_app.py"]



