import requests

# Utilisez l'adresse IP donnée
ip_address = "207.45.249.140"
url = f'http://ip-api.com/json/{ip_address}'

r = requests.get(url=url)
query = r.json()

# Vérifier si la requête a réussi
if query and query['status'] == 'success':
    geolocation_values = {
        "ip_adress_geopoint": f"POINT({query['lon']} {query['lat']})",
        "ip_adress_city": query['city'],
        "ip_adress_region_hierarchy": f"{query['regionName']},{query['city']}",
        "ip_adress_region": query['regionName'],
        "ip_adress_region_code": query['region'],  # Notez que l'API ne fournit pas directement un 'region_code'
        "ip_adress_country_code": query['countryCode'],
        "ip_adress_country": query['country'],
    }
    
    # Affichage des valeurs pour vérification
    for key, value in geolocation_values.items():
        print(f"{key}: {value}")
else:
    print("La requête de géolocalisation IP a échoué.")
