import os

# Supposons que la variable d'environnement s'appelle KAFKA_PASSWORD
kafka_password = os.getenv("KAFKA_PASSWORD")

from confluent_kafka import Producer

def lire_config_kafka(fichier_config):
    config = {}
    with open(fichier_config, 'r') as file:
        for line in file:
            if line.strip() and not line.startswith('#'):
                key, value = line.strip().split('=', 1)
                config[key] = value

    config["sasl.password"]= kafka_password
    return config


def instance_kafka(config_kafka):
    p = Producer({
        'bootstrap.servers': config_kafka.get('bootstrap.servers'),
        'security.protocol': config_kafka.get('security.protocol'),
        'sasl.mechanisms': config_kafka.get('sasl.mechanisms'),
        'sasl.username': kafka_password,
        'sasl.password': config_kafka.get('sasl.password')
    })
    return p

