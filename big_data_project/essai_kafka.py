import requests
from confluent_kafka import Producer
import time
import json
import random
import time
import ipaddress
from datetime import datetime

def lire_config_kafka(fichier_config):
    config = {}
    with open(fichier_config, 'r') as file:
        for line in file:
            if line.strip() and not line.startswith('#'):
                key, value = line.strip().split('=', 1)
                config[key] = value
    return config

# Charger la configuration Kafka
config_kafka = lire_config_kafka('kafka_config.config')

# Créer une instance du producteur Kafka
p = Producer({
    'bootstrap.servers': config_kafka.get('bootstrap.servers'),
    'security.protocol': config_kafka.get('security.protocol'),
    'sasl.mechanisms': config_kafka.get('sasl.mechanisms'),
    'sasl.username': config_kafka.get('sasl.username'),
    'sasl.password': config_kafka.get('sasl.password')
})



url = "https://api-56ce4d5f-779f448b-dku.eu-west-3.app.dataiku.io/public/api/v1/generate_rows/generate-rows/run"
response = requests.get(url)
data = response.json()



# Step 1: Rename 'client_addr' to 'ip_address'
data['response']['ip_address'] = data['response'].pop('client_addr')

# Step 2: Clear 'ip_address' if not valid
try:
    ipaddress.ip_address(data['response']['ip_address'])
except ValueError:
    data['response']['ip_address'] = None

# Step 3: Remove specified columns
for key in ['event_params', 'session_params', 'visitor_params']:
    data['response'].pop(key, None)

# Step 4: Geo-locate IP (This step requires an external service and is not shown here)

# Step 5 & 6: Parse 'server_ts' and extract components
server_ts_str = data['response']['server_ts']
server_ts_parsed = datetime.strptime(server_ts_str, '%Y-%m-%dT%H:%M:%S.%f')
data['response']['server_ts_parsed'] = server_ts_parsed.isoformat()
data['response']['date_components'] = {
    'year': server_ts_parsed.year,
    'month': server_ts_parsed.month,
    'day': server_ts_parsed.day,
    'hour': server_ts_parsed.hour,
    'minute': server_ts_parsed.minute,
    'second': server_ts_parsed.second
}

# Step 7 & 8: Round values in 'customer' and fill empty cells with '0'
# Assuming 'customer' should contain a numeric value
data['response']['customer'] = round(float(data['response']['customer'])) if data['response']['customer'] else '0'

# Convert back to JSON if needed
data_json = json.dumps(data)
print(data_json)

p.produce('topic_logs_website', key='key', value = data_json.encode('utf-8'))
p.flush()
