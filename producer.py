import requests
from confluent_kafka import Producer
import requests
import time
import json
import random
from ipaddress import ip_address
from datetime import datetime

from utils import lire_config_kafka, instance_kafka

import config
import dataikuapi

# Créer un client API en utilisant les configurations
api_model = dataikuapi.APINodeClient(config.API_MODEL_ENDPOINT, config.API_MODEL_ID)

# Utiliser l'URL de données en temps réel
url_real_time = config.URL_REAL_TIME_DATA


# Charger la configuration Kafka
config_kafka = lire_config_kafka('kafka_config.config')
# Créer une instance du producteur Kafka
p = instance_kafka(config_kafka)


def get_real_time_data():
    geolocation_values = {}

    new_data = requests.get(url_real_time)
    data = new_data.json()

    # Extract the relevant 'response' section of the JSON data
    response_data = data['response']

    # Step 1: Rename 'client_addr' to 'ip_address'
    response_data['ip_adress'] = response_data.pop('client_addr')

    # Step 2: Clear 'ip_address' if not valid
    try:
        ip_address(response_data['ip_adress'])
    except ValueError:
        response_data['ip_adress'] = None

    # Step 3: Remove specified columns
    for key in ['event_params', 'session_params', 'visitor_params']:
        response_data.pop(key, None)

    # Step 4: Geo-locate IP (This step requires an external service and is not shown here)

    ip_address_data = response_data['ip_adress']
    url = f'http://ip-api.com/json/{ip_address_data}'

    r = requests.get(url=url)
    query = r.json()

    # Vérifier si la requête a réussi
    if query and query['status'] == 'success':
        geolocation_values = {
            "ip_adress_geopoint": f"POINT({query['lon']} {query['lat']})",
            "ip_adress_city": query['city'],
            "ip_adress_region_hierarchy": f"{query['regionName']},{query['city']}",
            "ip_adress_region": query['regionName'],
            "ip_adress_region_code": query['region'],  # Notez que l'API ne fournit pas directement un 'region_code'
            "ip_adress_country_code": query['countryCode'],
            "ip_adress_country": query['country'],
        }
        
        # Affichage des valeurs pour vérification
        for key, value in geolocation_values.items():
            print(f"{key}: {value}")
    else:
        print("La requête de géolocalisation IP a échoué.")


    # Step 5 & 6: Parse 'server_ts' and extract components
    server_ts = response_data['server_ts']

    # Analyser la chaîne de date/heure pour obtenir un objet datetime
    server_ts_parsed = datetime.strptime(server_ts, "%Y-%m-%dT%H:%M:%S.%f")

    # Extraire les composants avant de convertir en format ISO
    server_ts_parsed_year = server_ts_parsed.year
    server_ts_parsed_month = server_ts_parsed.month
    server_ts_parsed_day = server_ts_parsed.day

    # Maintenant convertir en format ISO
    server_ts_zulu = server_ts_parsed.isoformat() + 'Z'

    # Créer un dictionnaire avec les valeurs
    timestamp_values = {
        "server_ts_parsed": server_ts_zulu,
        "server_ts_parsed_year": server_ts_parsed_year,
        "server_ts_parsed_month": server_ts_parsed_month,
        "server_ts_parsed_day": server_ts_parsed_day,
    }

    # Affichage des valeurs pour vérification
    for key, value in timestamp_values.items():
        print(f"{key}: {value}")


    # Step 7 & 8: Round values in 'customer' and fill empty cells with '0'
    # Assuming 'customer' should contain a numeric value
    response_data['customer'] = round(float(response_data['customer'])) if response_data['customer'] else '0'

    # Convert back to JSON if needed
    data_json = json.dumps(response_data)
    #print(data_json)

    # Fusionner 'geolocation_values' et 'timestamp_values' avec 'response_data'
    response_data.update(geolocation_values)
    response_data.update(timestamp_values)

    # Correction de la faute de frappe dans 'ip_adress' vers 'ip_address'
    if 'ip_adress' in response_data:
        response_data['ip_address'] = response_data.pop('ip_adress')

    # Ajouter les valeurs géolocalisées et les valeurs de timestamp à 'response_data'
    # Vous avez déjà les dictionnaires 'geolocation_values' et 'timestamp_values' prêts à l'emploi

    print(response_data)

    return response_data


def push_data_kafka(response_data):
    # Convertir 'response_data' mis à jour en JSON
    data_json = json.dumps(response_data, ensure_ascii=False)
    # Send the preprocessed data to Kafka
    p.produce('topic_website', key='key', value=data_json.encode('utf-8'))
    p.flush()


#push_data_kafka(get_real_time_data())