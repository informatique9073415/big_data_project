import dataikuapi

client = dataikuapi.APINodeClient("https://api-61123218-fad383b0-dku.eu-west-3.app.dataiku.io/", "website_logs")

record_to_predict ={"server_ts": "2014-03-14T04:08:33.045", "visitor_id": "c6a7f15172b3501", "session_id": "37b6a111f2c3d75", "location": "http://www.dataiku.com/products/shaker/", "referer": "dataiku", "user_agent": "Mozilla/5.0 (Windows NT 6.1; rv:27.0) Gecko/20100101 Firefox/27.0", "type": "page", "br_width": "1482", "br_height": "965", "sc_width": "1920", "sc_height": "1080", "br_lang": "en", "tz_off": "-540", "customer": 1, "timestamp": "2024-02-02 17:15:36.520147", "ip_adress_geopoint": "POINT(139.754 35.694)", "ip_adress_city": "Chiyoda", "ip_adress_region_hierarchy": "Tokyo,Chiyoda", "ip_adress_region": "Tokyo", "ip_adress_region_code": "13", "ip_adress_country_code": "JP", "ip_adress_country": "Japan", "server_ts_parsed": "2014-03-14T04:08:33.045000Z", "server_ts_parsed_year": 2014, "server_ts_parsed_month": 3, "server_ts_parsed_day": 14, "ip_address": "202.246.252.90"}

prediction = client.predict_record("khalid", record_to_predict)
print(prediction["result"])