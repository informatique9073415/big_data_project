import requests
from confluent_kafka import Producer
import time
import json
import random
import pandas as pd
from ipaddress import ip_address


def lire_config_kafka(fichier_config):
    config = {}
    with open(fichier_config, 'r') as file:
        for line in file:
            if line.strip() and not line.startswith('#'):
                key, value = line.strip().split('=', 1)
                config[key] = value
    return config

# Charger la configuration Kafka
config_kafka = lire_config_kafka('kafka_config.config')

# Créer une instance du producteur Kafka
p = Producer({
    'bootstrap.servers': config_kafka.get('bootstrap.servers'),
    'security.protocol': config_kafka.get('security.protocol'),
    'sasl.mechanisms': config_kafka.get('sasl.mechanisms'),
    'sasl.username': config_kafka.get('sasl.username'),
    'sasl.password': config_kafka.get('sasl.password')
})



# Get data from the API
url = "https://api-56ce4d5f-779f448b-dku.eu-west-3.app.dataiku.io/public/api/v1/generate_rows/generate-rows/run"
response = requests.get(url)
data = response.json()

# Extract the relevant 'response' section of the JSON data
response_data = data['response']

# Convert the response data to a pandas DataFrame
df = pd.DataFrame([response_data])  # Note that we pass a list with the response_data

# Define the is_valid_ip function
def is_valid_ip(ip):
    try:
        ip_address(ip)
        return True
    except ValueError:
        return False
    
# Preprocessing steps (assuming you have defined all necessary functions and logic)
df.rename(columns={'client_addr': 'ip_address'}, inplace=True)
df['ip_address'] = df['ip_address'].apply(lambda x: x if is_valid_ip(x) else None)
df.drop(columns=['event_params', 'session_params', 'visitor_params'], inplace=True)
df['server_ts_parsed'] = pd.to_datetime(df['server_ts'], errors='coerce')
df['year'] = df['server_ts_parsed'].dt.year
df['month'] = df['server_ts_parsed'].dt.month
df['day'] = df['server_ts_parsed'].dt.day
df['hour'] = df['server_ts_parsed'].dt.hour
df['minute'] = df['server_ts_parsed'].dt.minute
df['second'] = df['server_ts_parsed'].dt.second


# Convert the DataFrame back to JSON for Kafka
data_json = df.to_json(orient='records')

print(data_json)


# Send the preprocessed data to Kafka
p.produce('topic_website', key='key', value=data_json.encode('utf-8'))
p.flush()
