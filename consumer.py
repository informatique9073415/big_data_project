from confluent_kafka import Consumer, KafkaException, KafkaError
from utils import lire_config_kafka
from time import sleep
import json

# Charger la configuration Kafka
config_kafka = lire_config_kafka('kafka_config.config')

def create_consumer():
    # Configuration de base du consommateur Kafka
    config_kafka['group.id'] = 'my_website_logs_consumer'
    config_kafka['auto.offset.reset'] = 'earliest'

    # Créer une instance du consommateur Kafka
    consumer = Consumer({
        'bootstrap.servers': config_kafka.get('bootstrap.servers'),
        'security.protocol': config_kafka.get('security.protocol'),
        'sasl.mechanisms': config_kafka.get('sasl.mechanisms'),
        'sasl.username': config_kafka.get('sasl.username'),
        'sasl.password': config_kafka.get('sasl.password'),
        'group.id': config_kafka.get('group.id'),
        'auto.offset.reset': config_kafka.get('auto.offset.reset')

    })

    # S'abonner au sujet
    consumer.subscribe(['topic_website'])

    return consumer


def poll_message():
    # Créer un consommateur
    consumer = create_consumer()
    i = 0 
    try:
        while True:
            # Récupérer un message
            msg = consumer.poll(timeout=1.0)
            if msg is None:
                # No message available within timeout.
                # Initial message consumption may take up to
                # `session.timeout.ms` for the consumer group to
                # rebalance and start consuming
                print("Waiting for message or event/error in poll()")
                i += 1
                if i > 10:
                    print("Génération de nouvelles données")
                    return None
                    break

                continue
            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    # Fin du partition, pas de problème ici
                    continue
                    i += 1
                else:
                    print(msg.error())
                    break

            

            # Message reçu
            print(f"Message reçu: {msg.value().decode('utf-8')}")
            return json.loads(msg.value().decode('utf-8'))
            break
    except KeyboardInterrupt:
        pass
    finally:
        # Fermer le consommateur proprement
        consumer.close()

# Créer un consommateur
#consumer = create_consumer()

# Commencer à lire les messages
#poll_message()