record_to_predict = {
    "server_ts": "2014-03-05T01:54:35.745", # exemple de valeur de type date sous forme de string
    "server_ts_parsed": "2014-03-05", # exemple de valeur de type date
    "server_ts_parsed_year": 2014, # exemple de valeur de type bigint
    "server_ts_parsed_month": 3, # exemple de valeur de type bigint
    "server_ts_parsed_day": 5, # exemple de valeur de type bigint
    "ip_address": "108.208.74.190", # exemple de valeur de type string
    "ip_address_geopoint": "geo_point_data", # exemple de valeur de type string
    "ip_address_city": "city_name", # exemple de valeur de type string
    "ip_address_region_hierarchy": "region_hierarchy_data", # exemple de valeur de type string
    "ip_address_region": "region_name", # exemple de valeur de type string
    "ip_address_region_code": "region_code", # exemple de valeur de type string
    "ip_address_country_code": "country_code", # exemple de valeur de type string
    "ip_address_country": "country_name", # exemple de valeur de type string
    "visitor_id": "220cb851220d183", # exemple de valeur de type string
    "session_id": "a3f83071602ae2d", # exemple de valeur de type string
    "location": "http://dataiku.com/blog", # exemple de valeur de type string
    "referer": "dataiku", # exemple de valeur de type string
    "user_agent": "Mozilla/5.0", # exemple de valeur de type string
    "type": "page", # exemple de valeur de type string
    "br_width": 320, # exemple de valeur de type bigint
    "br_height": 460, # exemple de valeur de type bigint
    "sc_width": 320, # exemple de valeur de type bigint
    "sc_height": 568, # exemple de valeur de type bigint
    "br_lang": "en-us", # exemple de valeur de type string
    "tz_off": 300, # exemple de valeur de type bigint
    "customer": 0.0 # exemple de valeur de type double
}
